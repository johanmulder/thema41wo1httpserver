package nl.hanze.web.t41.http;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HTTPListener
{
	private int portnumber;
	private HTTPHandler httpHandler;

	public HTTPListener(int port, HTTPHandler hh) throws Exception
	{
		if (port < HTTPSettings.PORT_MIN || port > HTTPSettings.PORT_MAX)
			throw new Exception("Invalid TCP/IP port, out of range");
		this.portnumber = port;
		this.httpHandler = hh;
	}

	@SuppressWarnings("resource")
	public void startUp() throws Exception
	{
		final ServerSocket servsock = new ServerSocket(portnumber);
		System.out.println("Server started");
		System.out.println("Waiting requests at port " + portnumber);

		while (true)
		{
			final Socket s = servsock.accept();
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						System.err.println("Accepted connection on " + s.toString());
						httpHandler.handleRequest(s.getInputStream(), s.getOutputStream());
					}
					catch (IOException e)
					{
						// An IO exception occurred somewhere in the http handler.
						e.printStackTrace();
					}
					finally
					{
						try
						{
							if (s.isConnected())
								// Close the socket to prevent resource leaks.
								s.close();
						}
						catch (IOException e)
						{
							// Don't care, because the connection has already been closed.
						}
					}
				}
			}).start();
		}
	}
}
