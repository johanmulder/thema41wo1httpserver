package nl.hanze.web.t41.http;

import java.util.HashMap;
import java.util.Map;

/*
 * Static class for storing extensions and mimetypes.
 */
public class MimeType 
{
	// Constants for mime type definitions, because it's possible they're
	// used elsewhere in the application.
	public static final String TEXT_HTML = "text/html";
	public static final String TEXT_PLAIN = "text/plain";
	public static final String TEXT_CSS = "text/css";
	public static final String IMAGE_JPEG = "image/jpeg";
	public static final String IMAGE_PNG = "image/png";
	public static final String IMAGE_GIF = "image/gif";
	public static final String APPLICATION_JAVASCRIPT = "application/x-javascript";
	public static final String APPLICATION_PDF = "application/pdf";
	
	private static Map<String, String> mimeTypes;
		
	// Add the extensions and corresponding mimetypes 
	static 
	{
		mimeTypes = new HashMap<String, String>();
		
		mimeTypes.put("html", TEXT_HTML);
		mimeTypes.put("css", TEXT_CSS);
	
		mimeTypes.put("gif", IMAGE_GIF);
		mimeTypes.put("jpeg", IMAGE_JPEG);
		mimeTypes.put("jpg", IMAGE_JPEG);
		mimeTypes.put("jpe", IMAGE_JPEG);
		mimeTypes.put("png", IMAGE_PNG);
	
		mimeTypes.put("js", APPLICATION_JAVASCRIPT);
		
		mimeTypes.put("txt", TEXT_PLAIN);
		mimeTypes.put("pdf", APPLICATION_PDF);
	
	}
	
	/**
	 * Returns the right mimetype to a given extension
	 * @param extension The extension
	 * @return The mimetype
	 */
	 public static String getMimeTypeForExtension(String extension) {
		 return mimeTypes.get(extension);
	 }
}
