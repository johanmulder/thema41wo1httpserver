package nl.hanze.web.t41.http;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class HTTPSettings
{
	// Opgave 4: 1a
	// ZET HIER DE JUISTE DIRECTORY IN WAAR JE BESTANDEN STAAN.

	// static final String DOC_ROOT =
	// "C:/Users/apex/Documents/4.1/weekopgaven/wk1/httpserver";
	public static String DOC_ROOT = "/tmp/webtest";
	static final String FILE_NOT_FOUND = "404.html";

	static final int BUFFER_SIZE = 2048;
	static final int PORT_MIN = 0;
	static final int PORT_MAX = 65535;

	static public int PORT_NUM = 4444;

	public static String getDateTime()
	{
		// Return a standardized time string in GMT.
		DateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss zzz", Locale.US);
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		return format.format(new Date());
	}
}
