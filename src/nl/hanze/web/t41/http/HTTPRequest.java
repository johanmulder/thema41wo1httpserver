package nl.hanze.web.t41.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTTPRequest
{
	// HTTP request url.
	private String requestUrl = null;
	// HTTP request method.
	private String requestMethod = null;
	// HTTP version.
	private String httpVersion = null;
	// Request headers.
	private Map<String, String> headers;
	// The stream to read the request from.
	private InputStream in;

	public HTTPRequest(InputStream in) throws IOException
	{
		this.in = in;
		// Create a LinkedHashMap to maintain the order in which the
		// headers got sent to the http server.
		headers = new LinkedHashMap<>();
		// Read and parse the request.
		parseRequest(getRequestLines());
	}
	
	private List<String> getRequestLines() throws IOException
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String requestLine;
		List<String> lines = new ArrayList<String>();
		while ((requestLine = br.readLine()) != null)
		{
			if (requestLine.equals(""))
				// End of request.
				break;
			lines.add(requestLine);
		}
		
		return lines;
	}

	/**
	 * Parse a list of request lines.
	 * @param requestLines
	 */
	private void parseRequest(List<String> requestLines)
	{
		// No use in continuing if there are no request lines.
		if (requestLines.size() == 0)
			return;
		// Parse the request line.
		parseRequestLine(requestLines.get(0));
		// All following lines are request headers.
		for (int i = 1; i < requestLines.size(); i++)
			addHeader(requestLines.get(i));
	}
	
	/**
	 * Add a header to the map of headers.
	 * @param line The line containing the header.
	 */
	private void addHeader(String line)
	{
		// If there's no : in the line, it's no header line.
		int colonPos = line.indexOf(':');
		if (colonPos <= 0)
			return;
		String headerName = line.substring(0, colonPos);
		String headerValue = line.substring(colonPos + 1).trim();
		headers.put(headerName, headerValue);
	}
	
	/**
	 * Parse a request line.
	 * @param request The request line.
	 */
	private void parseRequestLine(String request)
	{
		// Match the http method, uri and http version.
		Pattern requestPattern = Pattern.compile("^([A-Z]+)\\s+(.*)\\s+HTTP/(\\d+\\.\\d+)$");
		Matcher matcher = requestPattern.matcher(request);
		if (matcher.find())
		{
			requestMethod = matcher.group(1);
			requestUrl = matcher.group(2);
			httpVersion = matcher.group(3);
		}
	}

	/**
	 * Get the request url.
	 * @return
	 */
	public String getRequestUrl()
	{
		return requestUrl;
	}

	/**
	 * Get the request method.
	 * @return
	 */
	public String getRequestMethod()
	{
		return requestMethod;
	}

	/**
	 * Get the HTTP version of the request.
	 * @return
	 */
	public String getHttpVersion()
	{
		return httpVersion;
	}

	public Map<String, String> getHeaders()
	{
		return headers;
	}
}
