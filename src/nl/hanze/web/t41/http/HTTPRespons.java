package nl.hanze.web.t41.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class HTTPRespons
{
	private OutputStream out;
	private HTTPRequest request;

	public HTTPRespons(OutputStream out)
	{
		this.out = out;
	}

	public void setRequest(HTTPRequest request)
	{
		this.request = request;
	}

	public void sendResponse() throws IOException
	{
		byte[] bytes = new byte[HTTPSettings.BUFFER_SIZE];
		FileInputStream fis = null;
		String fileName = request.getRequestUrl();

		try
		{
			File file = new File(HTTPSettings.DOC_ROOT, fileName);
			FileInputStream inputStream = getInputStream(file);

			out.write(getHTTPHeader(file));

			// Don't return unsupported files
			if (MimeType.getMimeTypeForExtension(getFileType(fileName)) != null)
			{ 
				// Read the contents of the file and output it to the output stream.
				int ch = inputStream.read(bytes, 0, HTTPSettings.BUFFER_SIZE);
				while (ch != -1)
				{
					out.write(bytes, 0, ch);
					ch = inputStream.read(bytes, 0, HTTPSettings.BUFFER_SIZE);
				}
			}

		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}
		finally
		{
			if (fis != null)
				fis.close();
		}
	}

	private FileInputStream getInputStream(File file)
	{
		try
		{
			// Open the file and return the inputstream
			return new FileInputStream(file);
		}
		catch (IOException e)
		{
			// e.printStackTrace();
		}
		
		return null;
	}

	private byte[] getHTTPHeader(File file) throws IOException
	{
		StringBuffer header = new StringBuffer();
		String mimeType = MimeType.getMimeTypeForExtension(getFileType(file.getCanonicalPath()));

		/*
		 * ** OPGAVE 4: 1b, 1c en 1d zorg voor een goede header: 200 als het
		 * bestand is gevonden; 404 als het bestand niet bestaat 500 als het
		 * bestand wel bestaat maar niet van een ondersteund type is
		 * 
		 * zorg ook voor ene juiste datum en tijd, de juiste content-type en de
		 * content-length.
		 */

		// Create a new file object with
		if (!file.exists() || !fileInDocumentRoot(file))
		{
			header.append("HTTP/1.1 404 Not found\r\n");
			mimeType = MimeType.TEXT_PLAIN;
		}
		else if (mimeType != null)
		{ 
			// File exists and mimetype is supported
			header.append("HTTP/1.1 200 OK\r\n"); // Create the first line
			header.append("Content-Length: " + file.length() + "\r\n");
		}
		else if (mimeType == null)
		{
			// Mime type is not supported.
			header.append("HTTP/1.1 500 Internal Server Error\r\n");
			mimeType = MimeType.TEXT_PLAIN;
		}

		// Add the content-type
		header.append("Content-Type: " + mimeType + "\r\n"); // Add mimetype
		// Always set the date.
		header.append("Date: " + HTTPSettings.getDateTime() + "\r\n");
		// End the header
		header.append("\r\n");

		System.out.println(header);
		return header.toString().getBytes();
	}

	/**
	 * Determine if the given file is in the HTTP document root.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private boolean fileInDocumentRoot(File file) throws IOException
	{
		// The canonical path of a file object is the actual location of
		// the file in the filesystem. If a client is attempting to go outside
		// the document root (for example by GETting /../../../etc/passwd),
		// the request will fail as it is not in the canonical path of the
		// document root
		return file.getCanonicalPath().startsWith(
				new File(HTTPSettings.DOC_ROOT).getCanonicalPath());
	}

	private String getFileType(String fileName)
	{
		int i = fileName.lastIndexOf(".");
		String ext = "";
		if (i > 0 && i < fileName.length() - 1)
			ext = fileName.substring(i + 1);

		return ext;
	}
}
