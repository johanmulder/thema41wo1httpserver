package nl.hanze.web.t41.http;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

public class HTTPHandlerImpl implements HTTPHandler
{

	@Override
	public void handleRequest(InputStream in, OutputStream out)
	{
		try
		{
			HTTPRequest request = new HTTPRequest(in);
			HTTPRespons respons = new HTTPRespons(out);
			respons.setRequest(request);

			showDateAndTime();
			System.out.println(": " + request.getRequestUrl());

			respons.sendResponse();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void showDateAndTime()
	{
		System.out.print(new Date().toString());
	}
}
