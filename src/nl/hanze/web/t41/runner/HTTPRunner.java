package nl.hanze.web.t41.runner;

import nl.hanze.web.t41.http.HTTPHandlerImpl;
import nl.hanze.web.t41.http.HTTPListener;
import nl.hanze.web.t41.http.HTTPSettings;

public class HTTPRunner
{
	public static void main(String args[])
	{
		/*
		 * ** OPGAVE 1.1 *** zorg ervoor dat het port-nummer en de
		 * basis-directory vanuit de command-line kunnen worden meegegeven. LET
		 * OP: de default-waarden moet je nog wel instellen in de
		 * Settings-klasse.
		 */
		
		if (args.length == 2)
		{
			HTTPSettings.DOC_ROOT = args[0];
			HTTPSettings.PORT_NUM = Integer.parseInt(args[1]);
		}

		try
		{
			HTTPListener listener = new HTTPListener(HTTPSettings.PORT_NUM, new HTTPHandlerImpl());
			listener.startUp();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
